import redis, time, string, random

from redis.sentinel import Sentinel

def id_generator(size=15, chars=string.ascii_uppercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

sentinel_host = ''
sentinel_port = '26379'
redis_master = ''
redis_pass = ''

i = 0

while True:
  time.sleep(1)
  try:
    sentinel = Sentinel([(sentinel_host, sentinel_port)], socket_timeout=0.1)
    redis_discover = sentinel.discover_master(redis_master)
    redis_client = redis.StrictRedis(host=redis_discover[0], port=int(redis_discover[1]), password=redis_pass, decode_responses=True)
    redis_key = "test-" + str.lower(id_generator(10))
    redis_value = str.lower(id_generator(10))
    redis_client.set(redis_key, redis_value)
    redis_client.get(redis_key)
  except Exception as e:
    print(f'Exception: {e}')
  print('ok')
  redis_client.close()
  